class RegistrationsController < Devise::RegistrationsController
  respond_to :json

  before_filter :require_no_authentication, :only => [:create ]


  def create
    logger.info params.inspect
    user = User.new(params_user)
    if user.save_with_confirmation
      sign_in('user', user)
      return render :json=> {:success => true, :email => user.email, :name => 'user', :token => user.authentication_token }, :status=>201
    else
      return render :json => user.errors, :status => 400
    end
  end





  private

  def params_user
    params[:email]||=''
    params[:password]||=''
    params[:password_confirmation]||=''
    params.permit(:email, :password, :password_confirmation)
  end


end