class ContactsController < ApplicationController

  skip_before_filter :verify_authenticity_token,
                     :if => Proc.new { |c| c.request.format == 'application/json' }

  before_filter :authenticate_user!
  #before_filter :user_needed!, :if => Proc.new { |c| c.request.format == 'application/json' }

  respond_to :json


  def index
    @contacts = current_user.contacts
    respond_with @contacts
  end


  def create
    @contact = current_user.contacts.build(params_contact)
    if @contact.save
      render json: @contact, status: :created, location: @contact
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end


  def show
    @contact = current_user.contacts.find(params[:id])
    respond_with @contact
  end


  def update
    @contact = current_user.contacts.find(params[:id])
    if @contact.update_attributes(params_contact)
      head :no_content
    else
      render json: @contact.errors, status: :unprocessable_entity
    end
  end


  def destroy
    @contact = current_user.contacts.find(params[:id])
    @contact.destroy
    head :no_content
  end

#-----------------------------------------------------------------------------------------------------------------------

  private


  def params_contact
    params[:contact][:name]||=""
    params[:contact][:email]||=""
    params[:contact][:country]||=""
    params[:contact][:phone]||=""
    params.require(:contact).permit(:name, :email, :country, :phone)
  end
end
