class SessionsController <  Devise::SessionsController
  respond_to :json

  before_filter :require_no_authentication, :only => [:create ]

  def create
    resource = User.find_for_database_authentication( :email=> params[:email] )
    return invalid_login_attempt unless resource
    if resource.valid_password?(params[:password])
      sign_in("user", resource)
      render :json=> {:success => true,
                      :auth_token => resource.authentication_token,
                      :email => resource.email,
                      :name =>  'user',
                      :message => 'You are successfully logged in'
        }
    else
      invalid_login_attempt
    end
  end


  def destroy
    sign_out(resource_name)
    render :json => {:success => "Successfully logged out"}
  end


  def get_current_user
    render :json => {email: current_user.email, name: 'user'}
  end

  #
  # protected
  #

  def invalid_login_attempt
    #warden.custom_failure!
    render :json=> {:success => false, :message => "Error with your login or password" }, :status => 401
  end

end