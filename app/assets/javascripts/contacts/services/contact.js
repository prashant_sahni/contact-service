app.factory("Contact", ['$resource', function($resource){
    return $resource('/api/contacts/:id', { id: "@id"},
   {
      'update':  { method: 'PUT' },
      'destroy': { method: 'DELETE' },
       'get': {method: 'GET',  isArray: true}
    });
}]);



//app.factory('flash', function($rootScope){
//    var queue = [];
//    var currentMessage = "";
//
//    $rootScope.$on("$routeChangeSuccess", function(){
//       currentMessage = queue.shift() || "";
//    });
//
//    return {
//        setMessage: function(message){
//          queue.push(message);
//        },
//        getMessage: function(){
//         return currentMessage;
//        }
//    }
//});



//      'create':  { method: 'POST'},
//      'index':   { method: 'GET',  isArray: true},
//      'show':    { method: 'GET',  isArray: false},