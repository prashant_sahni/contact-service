app.controller("LoginCtrl", ['$scope', '$rootScope', '$http', '$location', 'flash', function($scope, $rootScope, $http, $location, flash){

    if($rootScope.loggedInUser){
       var request = $http.post('/get_current_user');
       request.then(function(response){
          $scope.user = response.data;
       })
       $location.path('/contacts')
    }


    $scope.value = "Sign In"

    $scope.login = function(message){

       function success(response){
        var data = response.data;
        if(data){
          flash('success', data.message);
          $rootScope.loggedInUser = true;
          $scope.user = {email: data.email, name: data.name};
          $location.path("/");
        }
       }

        function failure(response){
          var data = response.data;
          if(data){
            flash('error', data.message);
            $rootScope.loggedInUser = false;
            $location.path("/login");
          }
       }

       var request = $http({method: 'post', url: '/sign_in', params: {email: $scope.email, password: $scope.password}});
       request.then(success, failure);
    }

   $scope.logout = function(){
    $http.delete('/sign_out');
    $rootScope.loggedInUser = false;
    flash('success', "You are successfully logged out");
    $location.path("/login");
   }

   $scope.signup = function(){
       $location.path('/signup');
   }
}]);