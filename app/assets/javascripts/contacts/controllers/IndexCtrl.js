app.controller('IndexCtrl', ['$scope', '$location', 'Contact', function($scope, $location, Contact){
   $scope.value = "Contact Listing" ;

   $scope.contacts = Contact.query();
   $scope.new = function(){
     $location.path('/contacts/new');
   }
   $scope.destroy = function(contact, index){
       function success(){
           $scope.contacts.splice(index,1);
       }
       function fail(){
         alert("FAIL!!");
        }
    Contact.destroy({id: contact.id}, success, fail);
   }
}]);