app.controller("SignUpCtrl", ['$scope', '$rootScope', '$http', '$location', 'flash' , function($scope, $rootScope, $http, $location, flash){
    $scope.submit = function(){


        function success(response){
            var data = response.data;
            if(data && data.success){
              $rootScope.loggedInUser = true;
              $scope.user = {email: data.email, name: data.name};
              $location.path('/contacts');
            }
        }



        function failure(response){
            _.each(response.data, function(errors, key){
                _.each(errors, function(e){
                    $scope.form[key].$dirty = true;
                    $scope.form[key].$setValidity(e, false);
                });
            });
        }

        $scope.form.$setPristine();
        var request = $http({method: 'post', url: '/sign_up', params: {email: $scope.email, password: $scope.password, password_confirmation: $scope.password_confirmation}});
        request.then(success, failure);
    }


    $scope.errorClass =  function(name){
        var s = $scope.form[name];
        return s.$invalid && s.$dirty ? 'error' : '';
    }

    $scope.errorMessage = function(name){
        var s = $scope.form[name].$error;
        result = [];
        _.each(s, function(key, value){
            result.push(value);
        });
        return result.join(", ");
    };

}]);