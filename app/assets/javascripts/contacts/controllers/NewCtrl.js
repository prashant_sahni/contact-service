app.controller('NewCtrl', ['$scope', '$routeParams', '$location', 'Contact', function($scope, $routeParams, $location, Contact){
    if($routeParams.id){
       $scope.form_heading = "Edit Contact Detail"
       $scope.contact = Contact.get({id: $routeParams.id})  ;
    }else{
        $scope.form_heading = "Add new contact"
        $scope.contact = new Contact();
    }

    $scope.submit = function(item, event){

       $scope.form.$setPristine();
       function success(response){
         $location.path('/contacts');
           $scope.submitted = false;
       }

       function removeErrors(){
         var elements = document.form.elements;
         _.each(elements, function(element){
             var key = element.name;
             if((element.attributes.getNamedItem('ng-model'))!= null){
                 $scope.form[key].$server_message = false;
             }
         });
       }

       function failure(response){
         // removeErrors();
         _.each(response.data, function(errors, key){
             _.each(errors, function(e){
               $scope.form[key].$dirty = true;
               $scope.form[key].$setValidity(e, false);
               $scope.form[key].$server_message = true;
            });
         });
       }

       if($routeParams.id){
           Contact.update($scope.contact, success, failure);
       }else{
          Contact.save($scope.contact, success, failure);
       }

    };

    $scope.errorClass =  function(name){
        var s = $scope.form[name];
        return s.$invalid && s.$dirty && s.$server_message ? 'error' : '';
    }

    $scope.errorMessage = function(name){
      var s = $scope.form[name].$error;
      result = [];
      _.each(s, function(key, value){
        result.push(value);
      });
     return result.join(", ");
    };

//    app.directive('formSubmitted', [function () {
//        return {
//            restrict: 'A',
//            require: 'form',
//            link: function (scope, element, attrs, ctrl) {
//                //ctrl.$submitted = false;
//                element.on('submit', function () {
//                    alert("hey");
//                });
//            }
//        };
//    }])

}]);