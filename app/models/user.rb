class User < ActiveRecord::Base


  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable

  before_create :generate_authentication_token

  has_many :contacts


  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      self.authentication_token= token
      break token unless User.where(authentication_token: token).first
    end
  end

  def save_with_confirmation
    self.skip_confirmation!
    self.confirmed_at = Time.now
    self.save
  end
end
