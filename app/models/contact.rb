class Contact < ActiveRecord::Base
  validates :name, :email, :country, :phone , :presence => true
  validates :email, format: {
    with: /^[^@]+@[^@]+\.[^@]+$/, :multiline => true
  }, unless: lambda{|o| o.email.blank?}

  belongs_to :user

end
