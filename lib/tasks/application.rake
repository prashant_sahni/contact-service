namespace :db do
  task :insert_users => :environment do
    user = User.new(:email => 'admin@contactify.com', :password => 'passw0rd', :password_confirmation => 'passw0rd')
    if user.valid?
      user.skip_confirmation!
      user.confirmed_at = Time.now

      user.save
    else
      puts user.errors.messages
    end
  end
end