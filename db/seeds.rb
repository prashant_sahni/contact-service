# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Contact.create(name: 'Prashant Sahni', email: 'prashantsahni5@gmail.com', country: 'India')
Contact.create(name: 'Nishutosh', email: 'om.nishutosh@gmail.com', country: 'India')
Contact.create(name: 'Bill', email: 'bill@yahoo.com', country: 'United States')
Contact.create(name: 'Catherene', email: 'cath@cancapital.com', country: 'United States')
Contact.create(name: 'Ian', email: 'ian@atst.com', country: 'Austrailia')