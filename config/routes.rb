Contactify::Application.routes.draw do

  root :to => 'layouts#index'

  devise_for :users, :controllers => {'sessions' => 'sessions', 'registrations' => 'registrations'} do
  end

  devise_scope :user do
	  post '/sign_up' => 'registrations#create', defaults: {format: 'json'}
	  post '/sign_in'    => 'sessions#create' ,  defaults: { format: 'json' }
	  delete '/sign_out' => 'sessions#destroy',  defaults: { format: 'json' }
	  post '/get_current_user' => 'sessions#get_current_user', defaults: { format: 'json' }
  end


  scope "api" do
    resources :contacts, defaults: { format: 'json' }
  end

  # Rewrite every other url
  constraints ->(request){ puts "In html: #{request.format}"; request.format == :html } do
    match "*path" => "layouts#index", :via => [:get, :post]
  end

end
