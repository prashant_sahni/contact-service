# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Contactify::Application.config.secret_key_base = 'ac0d3e27559551506d5da0ef713e52ec4a94032b6039f119e04e7c08a2a9d26b9eaaf2896c0fea72e4f64f57aedd947edbd11903d1edae61cd8481b7e9b32eca'
